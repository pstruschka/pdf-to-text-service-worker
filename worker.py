#!/usr/bin/env python3
import logging
import os
import subprocess
import uuid
import json

import bson
from rabbit import Rabbit
from object_store import MinioObjectStore as ObjectStore

LOG = logging

RABBIT_HOST = os.getenv('RABBIT_HOST', 'localhost')
WORKER_QUEUE = os.getenv('WORKER_QUEUE', 'worker')
STATUS_QUEUE = os.getenv('STATUS_QUEUE', 'update_status')

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG, format='$(name)-32s - $(levelname)-8s - %(message)-s')


def create_status(**kwargs):
    return json.dumps(kwargs)


def run_task(logger, object_store, task):
    bucket = task.get('bucket')
    pdf_name = task.get('object')

    # get pdf from object store
    object_store.download_object(bucket, pdf_name, pdf_name)

    status = subprocess.call(['pdftotext', '-layout', pdf_name])
    if status != 0:
        logger.warning('process failed')
        os.remove(pdf_name)
        return False

    # upload result txt to object store
    txt_name = pdf_name[0:-3] + 'txt'
    object_store.upload_object(bucket, txt_name, txt_name)
    os.remove(pdf_name)
    os.remove(txt_name)
    return True


def handle_message(channel, method, properties, body, logger, object_store):
    logger.debug('Recieved %r', body)
    try:
        task = bson.loads(body)
    except bson.struct.error:
        logger.warning('Invalid BSON')
        channel.basic_ack(delivery_tag=method.delivery_tag)
        return
    else:
        bucket_base_name = task.get('bucket')[:-37]

        if run_task(logger, object_store, task):
            channel.basic_publish(
                exchange='',
                routing_key=STATUS_QUEUE,
                body=create_status(
                    bucket=bucket_base_name,
                    object=task.get('object'),
                    status='processed'))
        else:
            channel.basic_publish(
                exchange='',
                routing_key=STATUS_QUEUE,
                body=create_status(
                    bucket=bucket_base_name,
                    object=task.get('object'),
                    status='error',
                    msg='processing failed'))
        channel.basic_ack(delivery_tag=method.delivery_tag)


def main():
    LOG.info('Stating a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Connect to RabbitMQ')

    if not (WORKER_QUEUE and STATUS_QUEUE):
        raise ValueError(
            f"WORKER_QUEUE set to {WORKER_QUEUE} and STATUS_QUEUE set to {STATUS_QUEUE}"
        )

    rabbit = Rabbit(RABBIT_HOST, WORKER_QUEUE, STATUS_QUEUE, INSTANCE_NAME)
    object_store = ObjectStore()
    rabbit.define_consumer(
        handle_message, logger=named_logging, object_store=object_store)

    named_logging.info("start_consuming")
    rabbit.start_consuming()


if __name__ == '__main__':
    main()
