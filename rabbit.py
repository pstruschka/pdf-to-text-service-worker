#!/usr/bin/env python3

import pika
import logging

LOG = logging

LOG.basicConfig(
    level=LOG.DEBUG, format='%(name)-32s - %(levelname)-8s - %(message)-s')

LOG.getLogger("pika").setLevel(logging.WARNING)

class Rabbit:
    def __init__(self, host, task_queue_name, status_queue_name, instance, attempts=5, delay=3):
        self._log = logging.getLogger('rabbit')
        self._host = host
        self._task_queue_name = task_queue_name
        self._status_queue_name = status_queue_name
        self._instance = instance
        self._connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host, connection_attempts=attempts, retry_delay=delay))
        self._channel = self._connection.channel()
        self._channel.queue_declare(queue=task_queue_name, durable=True)
        self._channel.queue_declare(queue=status_queue_name, durable=True)
        self._channel.basic_qos(prefetch_count=1)

    def define_consumer(self, callback, **kwargs):
        if not all((k in kwargs.keys()) for k in ['logger', 'object_store']):
            raise TypeError
        self._channel.basic_consume(
            lambda ch, method, properties, body:
            callback(ch, method, properties, body, **kwargs),
            queue=self._task_queue_name, consumer_tag=self._instance)

    def start_consuming(self):
        self._channel.start_consuming()
