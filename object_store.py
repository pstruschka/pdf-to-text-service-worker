'''
Object store adapter to minio
'''
import os

from minio import Minio
from minio.error import ResponseError


class MinioObjectStore():
    '''
    This
    '''
    def __init__(self):
        host = os.getenv('MINIO_ENDPOINT', '127.0.0.1:9000')
        access_key = os.getenv('MINIO_ACCESS_KEY', '')
        secret_key = os.getenv('MINIO_SECRET_KEY', '')
        if not (access_key and secret_key):
            raise ValueError("Some keys have not been passed")
        self.client = Minio(
            host,
            access_key=access_key,
            secret_key=secret_key,
            secure=False)

    def download_object(self, bucket, obj, dest):
        '''
        Dowloads an object
        '''
        try:
            self.client.fget_object(bucket, obj, dest)
        except ResponseError as err:
            print(err)

    def stat_object(self, bucket, obj):
        '''
        Checks that an object exists
        '''
        try:
            return bool(self.client.stat_object(bucket, obj))
        except ResponseError:
            return False

    def upload_object(self, bucket, obj, src):
        '''
        Uploads an object
        '''
        try:
            self.client.fput_object(bucket, obj, src)
        except ResponseError as err:
            print(err)
